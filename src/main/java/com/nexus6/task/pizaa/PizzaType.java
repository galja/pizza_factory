package com.nexus6.task.pizaa;

public enum PizzaType {
  Pepperoni, Cheese, Clam, Veggie
}
