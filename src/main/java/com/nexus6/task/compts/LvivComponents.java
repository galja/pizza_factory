package com.nexus6.task.compts;

import java.util.Arrays;

public class LvivComponents implements Addable {
  private int dough;
  private int sauce;
  private int cheese;
  private int[] topings;

  public LvivComponents() {
    dough = 5;
    sauce = 3;
    cheese = 7;
    topings = new int[]{4, 4, 4};
  }

  public int addDough() {
    return dough;
  }

  public int addSauce() {
    return sauce;
  }

  public int addCheese() {
    cheese += Arrays.stream(topings)
        .reduce(0, (acc, t) -> acc + t);
    return cheese;
  }
}
