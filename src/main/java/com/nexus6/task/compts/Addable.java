package com.nexus6.task.compts;

public interface Addable {
  int addDough();
  int addSauce();
  int addCheese();
}
